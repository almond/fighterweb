execute "DB 1] create psql user" do
    cwd "/home/vagrant/"
    user "root"
    command "sudo -u postgres createuser -s vagrant; ls"
end

execute "DB 2] create databases" do
    cwd "/home/vagrant/"
    user "vagrant"
    command "createdb -T template_postgis vagrant; ls"
end