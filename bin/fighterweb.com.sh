#! /usr/bin/zsh
set -e
LOGFILE=/srv/www/fighterweb/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=4

USER=almondev
GROUP=almondev

source /home/almondev/.virtualenvs/fighter/bin/activate
cd /srv/www/fighterweb/app
test -d $LOGDIR || mkdir -p $LOGDIR

exec python manage.py run_gunicorn -b 0.0.0.0:8001 -w $NUM_WORKERS -t 1200 --user=$USER --settings fighterweb.settings.prod --group=$GROUP --log-level=debug --log-file=$LOGFILE 2>>$LOGFILE
