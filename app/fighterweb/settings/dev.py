import platform
from .base import *

INSTALLED_APPS += ("debug_toolbar",)
INTERNAL_IPS = ("127.0.0.1",)

MIDDLEWARE_CLASSES += ("debug_toolbar.middleware.DebugToolbarMiddleware",)

# Turn off that annoying redirect... at least until it is necessary
DEBUG_TOOLBAR_CONFIG = {
	"INTERCEPT_REDIRECTS": False,	
}

THUMBNAIL_DEBUG = DEBUG

if platform.system() == "Windows":
	DATABASES = {
	    'default': {
	        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
	        'NAME': "fighterweb",                      # Or path to database file if using sqlite3.
	        # The following settings are not used with sqlite3:
	        'USER': 'fighterweb',
	        'PASSWORD': 'Mariohasataco.',
	        'HOST': 'localhost',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
	        'PORT': '',                      # Set to empty string for default.
	    }
	}

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
