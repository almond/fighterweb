import dj_database_url

DEBUG = True
TEMPLATE_DEBUG = DEBUG
DATABASES = {'default':  dj_database_url.config(default='postgis://vagrant:@10.0.0.20:5432/vagrant')}

PRODUCTION = False
