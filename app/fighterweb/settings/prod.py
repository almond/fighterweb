from .base import *

ASSETS_PATH = Path(__file__).ancestor(4)
MEDIA_ROOT = ASSETS_PATH.child("media")
STATIC_ROOT = ASSETS_PATH.child("static")

INSTALLED_APPS += ("gunicorn", ) 

ALLOWED_HOSTS = ["fighterweb.almonddev.com",]

DEBUG = True
TEMPLATE_DEBUG = DEBUG

EMAIL_BACKEND = "django_sendmail_backend.backends.EmailBackend"
