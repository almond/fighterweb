from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# Fighterweb apps
from home.views import IndexView

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^events/', include('events.urls', namespace="events", app_name="events")),
    url(r'^profiles/', include('accounts.urls', namespace="profiles", app_name="profiles")),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {"next_page": '/'}, name="logout"),
    url(r'^accounts/', include('django.contrib.auth.urls', namespace="auth", app_name="auth")),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^news/', include('news.urls', namespace="news", app_name="news")),
    url(r'^contact/', include('contact.urls', namespace="contact", app_name="contact")),
    url(r'^$', IndexView.as_view(), name='index'),
)

# If on development, use django to server MEDIA_ROOT files
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
