from django.conf.urls import patterns, url


urlpatterns = patterns(
    'accounts.views',
    url(r'create/$', 'create_user', name='create_user'),
    url(r'athletes/(?P<pk>\d+)/$', 'detail_athlete', name='detail_athlete'),
    url(r'promoters/(?P<pk>\d+)/$', 'detail_promoter', name='detail_promoter'),
    url(r'^athletes/$', 'list_athletes', name='list_athletes'),
    url(r'^promoters/$', 'list_promoters', name='list_promoters'),
)
