from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from .models import Athlete, Promoter
from .forms import UserForm


class AthleteListView(ListView):
    """
    Display athletes and stuff. Pretty neat
    """
    model = Athlete
    template_name = "accounts/list_athletes.html"


class AthleteDetailView(DetailView):
    model = Athlete
    template_name = "accounts/detail_athlete.html"


class PromoterListView(ListView):
    """
    Pretty much the same as above... but for promoters
    """
    model = Promoter
    template_name = "accounts/list_promoters.html"


class PromoterDetailView(DetailView):
    model = Promoter
    template_name = "accounts/detail_promoter.html"


class AccountCreateView(CreateView):
    model = get_user_model()
    form_class = UserForm
    template_name = "accounts/create_user.html"

    def get_success_url(self):
        return reverse_lazy("auth:login")


create_user = AccountCreateView.as_view()
list_athletes = AthleteListView.as_view()
detail_athlete = AthleteDetailView.as_view()
list_promoters = PromoterListView.as_view()
detail_promoter = PromoterDetailView.as_view()
