from django import forms
from django.contrib.auth import get_user_model


class UserForm(forms.ModelForm):
    password1 = forms.CharField(max_length=200, required=True,
                                widget=forms.PasswordInput(), label="Password")
    password2 = forms.CharField(max_length=200, required=True,
                                widget=forms.PasswordInput(), label="Confirm password")
    class Meta:
        model = get_user_model()
        fields = ("username", "first_name",
                  "last_name", "email")

    def clean_username(self):
        """
        Does the username exist?
        """
        username = self.cleaned_data["username"]
        try:
            get_user_model().objects.get(username=username)
        except get_user_model().DoesNotExist:
            return username
        raise forms.ValidationError("Username already taken.")

    def clean_password2(self):
        """
        Check that passwords match
        """
        password1 = self.cleaned_data["password1"]
        password2 = self.cleaned_data["password2"]
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password2"])
        if commit:
            user.save()
        return user



