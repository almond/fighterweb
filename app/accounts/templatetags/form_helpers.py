from django import template

register = template.Library()

@register.filter(name="add_class")
def add_class(tag, class_names):
    """
    Can't believe Django doesn't offer
    this by default
    """
    return tag.label_tag(attrs={"class": class_names})
    
