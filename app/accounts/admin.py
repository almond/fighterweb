from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import BaseUser, Athlete, Promoter


# Make user corrections so that they can be created in the admin backen thing
class BaseUserCreationForm(forms.ModelForm):
        """We overrode some stuff in the user model.. We need to tell the admin
           page how stuff works now.
        """
        password1 = forms.CharField(label="Password",
                widget=forms.PasswordInput)

        password2 = forms.CharField(label="Password Confirmation",
                widget=forms.PasswordInput)


        class Meta:
                model = BaseUser

        def clean_password2(self):
                password1 = self.cleaned_data.get("password1")
                password2 = self.cleaned_data.get("password2")
                if password1 and password2 and password1 != password2:
                        msg = "Passwords don't match"
                        raise forms.ValidationError(msg)

        def save(self, commit=True):
                user = super(BaseUserCreationForm, self).save(commit=False)
                user.set_password(self.cleaned_data["password1"])
                if commit:
                        user.save()
                return user

class BaseUserChangeForm(forms.ModelForm):
        """ Change user stuff and shit
        """
        password = ReadOnlyPasswordHashField()

        class Meta:
                model = BaseUser

        def clean_password(self):
                return self.initial["password"]


class BaseUserAdmin(UserAdmin):
        """Admin class thing"""
        add_form = BaseUserCreationForm
        form = BaseUserChangeForm


admin.site.register(BaseUser, BaseUserAdmin)
admin.site.register(Athlete)
admin.site.register(Promoter)
