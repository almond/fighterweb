import time

from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django_extensions.db.fields import AutoSlugField


class BaseUser(AbstractUser):

    @property
    def get_groups(self):
        """@todo: Return the groups this user belongs to.
        This to make accessing and filtering by associations easier
        probably redundant. Still thinking about it.

        """
        pass


class Promoter(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    slug = AutoSlugField(populate_from='company_name', blank=True, null=True)
    company_name = models.CharField(max_length=255, default="Independent")
    picture = models.ImageField(upload_to="promoters/%s/" % time.time())
    bio = models.TextField(blank=True, null=True)

    def __unicode__(self):
        """ Present a fight promoter by their names and company affiliation """
        return "%s %s" % (self.user.first_name, self.user.last_name)

    def get_absolute_url(self):
        """
        Guess what it does...
        """
        return reverse("profiles:detail_promoter", kwargs={"pk": self.pk})

    class Meta:
        verbose_name = "Promoter"
        verbose_name_plural = "Promoters"


class Athlete(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    slug = AutoSlugField(populate_from='nickname', blank=True, null=True)
    nickname = models.CharField(max_length=255)
    wins = models.IntegerField()
    loses = models.IntegerField()
    bio = models.TextField(blank=True, null=True)

    # Fighter image and stuff
    picture = models.ImageField(upload_to="athletes/%s/" % time.time())

    def __unicode__(self):
        """ Return full name with nickname if present """
        return '%s "%s" %s' % (self.user.first_name, self.nickname, self.user.last_name)

    def get_absolute_url(self):
        """
        Guess what it does...
        """
        return reverse("profiles:detail_athlete", kwargs={"pk": self.pk})

    class Meta:
        verbose_name = "Athlete"
        verbose_name_plural = "Athletes"

