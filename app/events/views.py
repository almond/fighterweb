# Django imports
import json
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.views.generic import ListView, DetailView, CreateView

# Fighterweb imports
from events.forms import EventAddTicketForm, FightForm
from .models import Event, FightType, Fight
from .forms import EventForm
from stores.forms import TicketVariantForm
from stores.models import Ticket, TicketVariant


class EventListView(ListView):
    """ Display events by various parameters """
    model = Event
    template_name = "events/list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(EventListView, self).get_context_data(*args, **kwargs)
        # Get what fighttypes are available
        context["fight_types"] = FightType.objects.filter(
            fight__event__in=context["object_list"]
        )
        return context


class EventDetailView(DetailView):
    model = Event
    template_name = "events/detail.html"


class EventShopView(DetailView):
    model = Event
    template_name = "events/shop.html"


class EventCreateView(CreateView):
    model = Event
    form_class = EventForm
    template_name = "events/create.html"

    def get_success_url(self):
        # Pass the created event as a get parameter for subsecuent steps
        return reverse_lazy('events:add_tickets', kwargs={"slug": self.object.slug})


def __get_master_ticket(event):
    # Get the event master ticket.. or create it if non existant
    try:
        ticket = Ticket.objects.get(event=event)
    except Ticket.DoesNotExist:
        ticket = Ticket(event=event, name=event.name,
                        slug=event.slug, description=event.description)
        ticket.save()

    return ticket


def ajax_return_ticket_form(request, slug=None):
    if not slug:
        raise Http404

    event = Event.objects.get(slug=slug)
    ticket = __get_master_ticket(event)

    # Now render the damn form


def add_tickets_to_event(request, slug):
    """
    On get request, render the template loaded with
    the just created event thing there. On an ajax
    post request create tickets relevant to said event
    :param request:
    :param slug:
    """

    event = get_object_or_404(Event, slug=slug)
    ticket = __get_master_ticket(event)

    if request.is_ajax():
        ticket_form = TicketVariantForm(request.POST, instance=TicketVariant(ticket=ticket))
        if ticket_form.is_valid():
            ticket_form.save()
            return HttpResponse(json.dumps({"status": True, "message": "Success"}), mimetype="application/json")
        else:
            # Actually return meaningfull errors
            return HttpResponse(json.dumps({"status": False, "message": "Error"}), mimetype="application/json")

    if request.method == "POST":
        # Todo: Make it work without JS... Not sure why though...
        # I'll have to remove everything ticket related from here
        # Ticket creation will only be through JS
        event_form = EventAddTicketForm(request.POST, instance=event)
        if event_form.is_valid():
            event_form.save()
            return HttpResponseRedirect(reverse("events:add_shows", kwargs={"slug": event.slug}))
    else:
        event_form = EventAddTicketForm(instance=event)
        ticket_form = TicketVariantForm(instance=TicketVariant(ticket=ticket))

    # Get the ticket model form
    return render_to_response("events/addtickets.html",
                              RequestContext(request, {"event_form": event_form,
                                                       "ticket_form": ticket_form,
                                                       "event": event}))


def add_shows_to_event(request, slug):
    event = get_object_or_404(Event, slug=slug)

    if request.is_ajax():
        fight_form = FightForm(request.POST, instance=Fight(event=event))
        messages = {"status": False, "message": "Error"}
        if fight_form.is_valid():
            fight_form.save()
            messages["status"] = True
            messages["message"] = "Success"

        return HttpResponse(json.dumps(messages), mimetype="application/json")
    else:
        fight_form = FightForm(instance=Fight(event=event))

    return render_to_response("events/shows.html",
                              RequestContext(request, {"fight_form": fight_form, "event": event}))


list_event = EventListView.as_view()
create_event = EventCreateView.as_view()
shop_event = EventShopView.as_view()
detail_event = EventDetailView.as_view()
