from django.contrib import admin
from .models import Event, Fight, FightType


class FightInline(admin.TabularInline):
    model = Fight
    extra = 1


class EventAdmin(admin.ModelAdmin):
    inlines = [FightInline,]


class FightTypeAdmin(admin.ModelAdmin):
    pass


admin.site.register(Event, EventAdmin)
admin.site.register(FightType, FightTypeAdmin)
