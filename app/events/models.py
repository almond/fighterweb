import time
from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.gis.db import models as geomodels
from django.db.models import Min, Max
from django.template.loader import get_template
from django.template import Context
from django_extensions.db.fields import AutoSlugField
from localflavor.us.us_states import STATE_CHOICES


class Event(models.Model):
    SEATING_CHOICES = (
        (u'Open seating venue', u'Open seating venue',),
        (u'Has reserved seating', u'Has reserved seating',),
    )
    SERVICE_FEES = (
        (u'I will pay service fees', u'I will pay service fees'),
        (u'Split service fees', u'Split service fees'),
        (u'Add services fees to ticket price', u'Add service fees to ticket price'),
    )
    TICKETING_CHOICES = (
        (u'No tickets required', u'No tickets required'),
        (u'Tickets required', u'Tickets required'),
    )

    promoter = models.ForeignKey('accounts.Promoter', blank=True, null=True)
    name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='name', blank=True, null=True)
    starts = models.DateTimeField()
    description = models.TextField()
    short_description = models.CharField(max_length=140)
    seating = models.CharField(choices=SEATING_CHOICES, max_length=140)

    # Geographic information
    zip_code = models.IntegerField(max_length=10)
    venue = models.CharField(max_length=100)
    state = models.CharField(max_length=100, choices=STATE_CHOICES)
    city = models.CharField(max_length=100)
    point = geomodels.PointField(blank=True, null=True)

    # Fullfillment options
    ticketing = models.CharField(choices=TICKETING_CHOICES, max_length=100,
                                 blank=True, null=True)

    service_fees = models.CharField(choices=SERVICE_FEES, max_length=100,
                                    blank=True, null=True)

    # Display options
    is_published = models.BooleanField(default=False)

    # There will be a multistep form to enter events
    # Set this to true once all steps were completed
    is_complete = models.BooleanField(default=False)

    # Well.. if this event is in the past, set to True and archive
    has_ended = models.BooleanField(default=False)

    # Cover photo
    picture = models.ImageField(upload_to="events/%s/" % time.time(), blank=True, null=True)

    objects = geomodels.GeoManager()

    # def __unicode__(self):
    #     return u"%s presents %s" % (self.promoter.company_name, self.name)

    def get_absolute_url(self):
        return reverse("events:detail", kwargs={"slug": self.slug})

    def get_shop_url(self):
        return reverse("events:shop", kwargs={"slug": self.slug})


    # def get_lowest_price(self):
    #     """
    #     TODO: Do a one to one relationship. Will reduce a step here
    #     Get's the lowest price from an event by searching ticket
    #     variants
    #     """
    #     ticket = self.ticket_set.all()[0]
    #     lowest = ticket.ticketvariant_set.aggregate(lowest=Min("price"))["lowest"]
    #     return u"$%s" % lowest

    # def get_highest_price(self):
    #     """
    #     TODO: Do a one to one relationship. Will reduce a step here
    #     Get's the lowest price from an event by searching ticket
    #     variants
    #     """
    #     ticket = self.ticket_set.all()[0]
    #     highest = ticket.ticketvariant_set.aggregate(highest=Max("price"))["highest"]
    #     return u"$%s" % highest


class FightType(models.Model):
    """ What kind of fight is this? """
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s' % self.name


class Fight(models.Model):
    """ A show that contains fighters """
    LEVEL_CHOICES = (
        ("Professional", "Professional"),
        ("Amateur", "Amateur"),
    )
    fight_type = models.ForeignKey('FightType')
    event = models.ForeignKey('Event')
    level = models.CharField(max_length=100, choices=LEVEL_CHOICES)
    home = models.ForeignKey('accounts.Athlete', related_name="home")
    away = models.ForeignKey('accounts.Athlete', related_name="away")
    starts_at = models.TimeField()
    ends_at = models.TimeField()

    @property
    def duration(self):
        """
        How long will this fight take?
        """
        return str(self.ends - self.starts)

    def display(self):
        """
        Overriding the method thing.. Render a template with the
        fighters
        """
        context = {"home": self.home, "away": self.away}
        template = get_template("events/widgets/fight.html")
        return template.render(Context(context))

    def __unicode__(self):
        return u"%s" % (str(self.home) + "vs." + str(self.away))
