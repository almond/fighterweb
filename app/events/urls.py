from django.conf.urls import patterns, url


urlpatterns = patterns('events.views',
    url(r'^$', 'list_event', name='list'),
    url(r'^create/$', 'create_event', name='create'),
    url(r'^addtickets/(?P<slug>[\w-]+)/$', 'add_tickets_to_event', name='add_tickets'),
    url(r'^addshows/(?P<slug>[\w-]+)/$', 'add_shows_to_event', name='add_shows'),
    url(r'^(?P<slug>[\w-]+)/shop/$', 'shop_event', name='shop'),
    url(r'^(?P<slug>[\w-]+)/$', 'detail_event', name='detail'),
)
