from django import forms
from .models import Fight, Event

class FightForm(forms.ModelForm):
    """
    This form will need some mods. There are fields
    that shouldn't be displayed, event though they need
    to be modified
    """

    class Meta:
        model = Fight
        exclude = ("event",)


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        exclude = (
            'promoter',
        )


class EventAddTicketForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ("ticketing", "service_fees",)