from django import template

register = template.Library()


@register.filter(name="add_class_to_label")
def add_class_to_label(tag, class_name):
    """
    Adds a class to a field working around
    the retarded templates I have to deal with
    """
    return tag.label_tag(attrs={"class": class_name})


@register.filter(name="add_class_to_field")
def add_class_to_field(tag, class_name):
    """
    Same thing as before... I hate form customization crap
    """
    
    
    
