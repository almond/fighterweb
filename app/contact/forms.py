from django import forms
from django.core.mail import send_mail


class ContactForm(forms.Form):
    """
    Ugh
    """
    name = forms.CharField(max_length=100,
                    required=True,
                    widget=forms.TextInput(attrs={
                        "class": "field-name form_field",
                        "placeholder": "Name"
                    }))
    email = forms.EmailField(required=True,
                             widget=forms.TextInput(attrs={
                                 "class": "field-email form_field",
                                 "placeholder": "Email"
                             }))
    subject = forms.CharField(max_length=100,
                              required=True,
                              widget=forms.TextInput(attrs={
                                  "class": "field-subject form_field",
                                  "placeholder": "Subject"
                              }))
    message = forms.CharField(max_length=100,
                              required=True,
                              widget=forms.Textarea(attrs={
                                  "class": "field-message form_field",
                                  "placeholder": "Message"
                              }))

    def send_email(self):
        """
        Upon validation, send the requested email
        """
        send_mail(self.cleaned_data["subject"],
                  self.cleaned_data["message"],
                  self.cleaned_data["email"], ["admin@thefighterweb.com", ],
                  fail_silently=False)

        
