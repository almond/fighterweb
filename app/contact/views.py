from django.shortcuts import render, redirect
from .forms import ContactForm


def index(request):
    """
    Just display that damn contact form
    and make it work
    """
    if request.method == 'POST':
        form = ContactForm(request.POST, request.FILES)
        if form.is_valid():
            form.send_email()
            return redirect('index')

    form = ContactForm()
    return render(request, 'contact/contact.html', {'form': form})



