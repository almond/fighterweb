from fabric.api import *

env.user = "almondev"
env.hosts = ["almonddev.com", ]

def start_virtualenv():
    local("workon fighterweb")

# Local developent
def start_dev_server():
    local("python manage.py runserver --settings fighterweb.settings.dev")

def start_dev_server_z():
    local("python manage.py runserver --settings fighterweb.settings.dev 0.0.0.0:9000")

def start_dev_shell():
    local("python manage.py shell --settings fighterweb.settings.dev")

def run_dev_command(command_name=""):
    """Run a command with the settings thing already setup"""
    local("python manage.py %s --settings fighterweb.settings.dev" % command_name)

# Remote serving
def create_prod_superuser():
    """ Just run this command on remote server """
    with cd("/srv/www/fighterweb/app/"):
        with prefix("source /home/almondev/.virtualenvs/fighter/bin/activate"):
            run("python manage.py createsuperuser --settings fighterweb.settings.prod")

def run_prod_command(command_name=""):
    """ Just run this command on remote server """
    with cd("/srv/www/fighterweb/app/"):
        with prefix("source /home/almondev/.virtualenvs/fighter/bin/activate"):
            run("python manage.py %s --settings fighterweb.settings.prod" % command_name)

def restart_prod_server():
    """ Start a gunicorn instance using the supervisor daemon from the server """
    run("sudo supervisorctl restart fighterweb")

# Deploy and shit
def deploy(commit="true"):
    if commit == "true":
        local("git add .")
        local("git commit -a")
        local("git push")

    with cd("/srv/www/fighterweb/"):
        run("git pull")

    with cd("/srv/www/fighterweb/app"):
        with prefix("source /home/almondev/.virtualenvs/fighter/bin/activate"):
            run("python manage.py migrate --settings fighterweb.settings.prod")
            run("python manage.py collectstatic --settings fighterweb.settings.prod")
    restart_prod_server()

def print_path():
    """
    This is actually just to test git
    """
    pass
    
