from django.contrib import admin

from .models import *


class TshirtVariantInline(admin.TabularInline):
    model = TshirtVariant
    extra = 1


class TicketVariantInline(admin.TabularInline):
    model = TicketVariant
    extra = 1


class GenericProductVariantInline(admin.TabularInline):
    model = GenericProductVariant
    extra = 1


class TshirtAdmin(admin.ModelAdmin):
    inlines = [TshirtVariantInline,]


class TicketAdmin(admin.ModelAdmin):
    inlines = [TicketVariantInline,]
    


class GenericProductAdmin(admin.ModelAdmin):
    inlines = [GenericProductVariantInline, ]
        

admin.site.register(Tshirt, TshirtAdmin)
admin.site.register(Ticket, TicketAdmin)
admin.site.register(GenericProduct, GenericProductAdmin)
