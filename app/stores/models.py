from .abstract_models import *
from accounts.models import Athlete
from decimal import Decimal
from django.conf import settings
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from events.models import Event
import time


class Ticket(Product):
    """ Tickets! Have different seating and everything """
    event = models.ForeignKey(Event)

    def __unicode__(self):
        return u"%s" % (self.event.name,)
        


class Tshirt(Product):
    """ Tshirts by athletes """
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    merchant = generic.GenericForeignKey()
    picture = models.ImageField(upload_to="tshirts/%s/" % time.time())
    
    def __unicode__(self):
        return u"%s" % self.name


class GenericProduct(Product):
    """ Kind of defeats the purpose of 
    polymorphic products and custom made e-commerce software... but oh well 
    
    I actually have no idea what fields go here... so I'll just
    leave it as is. Later with migrations fields can be added
    """
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    merchant = generic.GenericForeignKey()
    picture = models.ImageField(upload_to="generic_product/%s/" % time.time())

    def __unicode__(self):
        return u"%s" % self.name


class TshirtVariant(Variant):
    """ Different settings for tshirts"""
    tshirt = models.ForeignKey(Tshirt)
    color = models.CharField(max_length=20)
    size = models.CharField(max_length=6)


class TicketVariant(Variant):
    """ What does a ticket have? """
    ticket = models.ForeignKey(Ticket)
    description = models.TextField()
    seating = models.CharField(max_length=30)


class GenericProductVariant(Variant):
    """ Same as above... """
    generic_product = models.ForeignKey(GenericProduct)


class Cart(models.Model):
    """ Well.. people have to add their shopping somewhere """
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    created = models.DateTimeField()
    updated = models.DateTimeField()
    status = models.CharField(max_length=20)


class CartItem(Item):
    cart = models.ForeignKey(Cart)


# Now the important order thing
class Order(models.Model):
    """ Well... we neeed to keep track of orders somehow """
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    balance = models.DecimalField(max_digits=10, decimal_places=2)
    total = models.DecimalField(max_digits=10, decimal_places=2)
    status = models.CharField(max_length=20)
    

class OrderItem(Item):
    """ Well... we have to know what's inside an order """
    order = models.ForeignKey(Order) 


class ShippingAddress(Address):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    directions = models.TextField()


class BillingAddress(Address):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
