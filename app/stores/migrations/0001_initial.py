# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Ticket'
        db.create_table(u'stores_ticket', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('weight', self.gf('django.db.models.fields.DecimalField')(default='1.00', max_digits=5, decimal_places=2)),
            ('height', self.gf('django.db.models.fields.DecimalField')(default='1.00', max_digits=5, decimal_places=2)),
            ('width', self.gf('django.db.models.fields.DecimalField')(default='1.00', max_digits=5, decimal_places=2)),
            ('depth', self.gf('django.db.models.fields.DecimalField')(default='1.00', max_digits=5, decimal_places=2)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['events.Event'])),
        ))
        db.send_create_signal(u'stores', ['Ticket'])

        # Adding model 'Tshirt'
        db.create_table(u'stores_tshirt', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('weight', self.gf('django.db.models.fields.DecimalField')(default='1.00', max_digits=5, decimal_places=2)),
            ('height', self.gf('django.db.models.fields.DecimalField')(default='1.00', max_digits=5, decimal_places=2)),
            ('width', self.gf('django.db.models.fields.DecimalField')(default='1.00', max_digits=5, decimal_places=2)),
            ('depth', self.gf('django.db.models.fields.DecimalField')(default='1.00', max_digits=5, decimal_places=2)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'stores', ['Tshirt'])

        # Adding model 'TshirtVariant'
        db.create_table(u'stores_tshirtvariant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sku', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('stock', self.gf('django.db.models.fields.IntegerField')()),
            ('can_backorder', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('tshirt', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stores.Tshirt'])),
            ('color', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('size', self.gf('django.db.models.fields.CharField')(max_length=6)),
        ))
        db.send_create_signal(u'stores', ['TshirtVariant'])

        # Adding model 'TicketVariant'
        db.create_table(u'stores_ticketvariant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sku', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('stock', self.gf('django.db.models.fields.IntegerField')()),
            ('can_backorder', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ticket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stores.Ticket'])),
            ('seating', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'stores', ['TicketVariant'])

        # Adding model 'Cart'
        db.create_table(u'stores_cart', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.BaseUser'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')()),
            ('updated', self.gf('django.db.models.fields.DateTimeField')()),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'stores', ['Cart'])

        # Adding model 'CartItem'
        db.create_table(u'stores_cartitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('total', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('cart', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stores.Cart'])),
        ))
        db.send_create_signal(u'stores', ['CartItem'])

        # Adding model 'Order'
        db.create_table(u'stores_order', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.BaseUser'])),
            ('balance', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('total', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'stores', ['Order'])

        # Adding model 'OrderItem'
        db.create_table(u'stores_orderitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('total', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stores.Order'])),
        ))
        db.send_create_signal(u'stores', ['OrderItem'])

        # Adding model 'ShippingAddress'
        db.create_table(u'stores_shippingaddress', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('street_address_1', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('street_address_2', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('zip_code', self.gf('django.db.models.fields.IntegerField')(max_length=6)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.BaseUser'])),
            ('directions', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'stores', ['ShippingAddress'])

        # Adding model 'BillingAddress'
        db.create_table(u'stores_billingaddress', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('street_address_1', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('street_address_2', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('zip_code', self.gf('django.db.models.fields.IntegerField')(max_length=6)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.BaseUser'])),
        ))
        db.send_create_signal(u'stores', ['BillingAddress'])


    def backwards(self, orm):
        # Deleting model 'Ticket'
        db.delete_table(u'stores_ticket')

        # Deleting model 'Tshirt'
        db.delete_table(u'stores_tshirt')

        # Deleting model 'TshirtVariant'
        db.delete_table(u'stores_tshirtvariant')

        # Deleting model 'TicketVariant'
        db.delete_table(u'stores_ticketvariant')

        # Deleting model 'Cart'
        db.delete_table(u'stores_cart')

        # Deleting model 'CartItem'
        db.delete_table(u'stores_cartitem')

        # Deleting model 'Order'
        db.delete_table(u'stores_order')

        # Deleting model 'OrderItem'
        db.delete_table(u'stores_orderitem')

        # Deleting model 'ShippingAddress'
        db.delete_table(u'stores_shippingaddress')

        # Deleting model 'BillingAddress'
        db.delete_table(u'stores_billingaddress')


    models = {
        u'accounts.baseuser': {
            'Meta': {'object_name': 'BaseUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'accounts.promoter': {
            'Meta': {'object_name': 'Promoter'},
            'company_name': ('django.db.models.fields.CharField', [], {'default': "'Independent'", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'events.event': {
            'Meta': {'object_name': 'Event'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end_on': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_tickets': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'promoter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.Promoter']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'start_on': ('django.db.models.fields.DateTimeField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'venue': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'zip_code': ('django.db.models.fields.IntegerField', [], {'max_length': '10'})
        },
        u'stores.billingaddress': {
            'Meta': {'object_name': 'BillingAddress'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'street_address_1': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'street_address_2': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"}),
            'zip_code': ('django.db.models.fields.IntegerField', [], {'max_length': '6'})
        },
        u'stores.cart': {
            'Meta': {'object_name': 'Cart'},
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"})
        },
        u'stores.cartitem': {
            'Meta': {'object_name': 'CartItem'},
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.Cart']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'total': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'})
        },
        u'stores.order': {
            'Meta': {'object_name': 'Order'},
            'balance': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'total': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"})
        },
        u'stores.orderitem': {
            'Meta': {'object_name': 'OrderItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.Order']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'total': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'})
        },
        u'stores.shippingaddress': {
            'Meta': {'object_name': 'ShippingAddress'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'directions': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'street_address_1': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'street_address_2': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"}),
            'zip_code': ('django.db.models.fields.IntegerField', [], {'max_length': '6'})
        },
        u'stores.ticket': {
            'Meta': {'object_name': 'Ticket'},
            'depth': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Event']"}),
            'height': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'})
        },
        u'stores.ticketvariant': {
            'Meta': {'object_name': 'TicketVariant'},
            'can_backorder': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'seating': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'stock': ('django.db.models.fields.IntegerField', [], {}),
            'ticket': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.Ticket']"})
        },
        u'stores.tshirt': {
            'Meta': {'object_name': 'Tshirt'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'depth': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'height': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'})
        },
        u'stores.tshirtvariant': {
            'Meta': {'object_name': 'TshirtVariant'},
            'can_backorder': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'size': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'stock': ('django.db.models.fields.IntegerField', [], {}),
            'tshirt': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.Tshirt']"})
        }
    }

    complete_apps = ['stores']