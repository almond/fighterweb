# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'GenericProductVariant.generic_product'
        db.add_column(u'stores_genericproductvariant', 'generic_product',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['stores.GenericProduct']),
                      keep_default=False)

        # Adding field 'GenericProduct.content_type'
        db.add_column(u'stores_genericproduct', 'content_type',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['contenttypes.ContentType']),
                      keep_default=False)

        # Adding field 'GenericProduct.object_id'
        db.add_column(u'stores_genericproduct', 'object_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=1),
                      keep_default=False)

        # Adding field 'GenericProduct.picture'
        db.add_column(u'stores_genericproduct', 'picture',
                      self.gf('django.db.models.fields.files.ImageField')(default=1, max_length=100),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'GenericProductVariant.generic_product'
        db.delete_column(u'stores_genericproductvariant', 'generic_product_id')

        # Deleting field 'GenericProduct.content_type'
        db.delete_column(u'stores_genericproduct', 'content_type_id')

        # Deleting field 'GenericProduct.object_id'
        db.delete_column(u'stores_genericproduct', 'object_id')

        # Deleting field 'GenericProduct.picture'
        db.delete_column(u'stores_genericproduct', 'picture')


    models = {
        u'accounts.baseuser': {
            'Meta': {'object_name': 'BaseUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'accounts.promoter': {
            'Meta': {'object_name': 'Promoter'},
            'company_name': ('django.db.models.fields.CharField', [], {'default': "'Independent'", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'events.event': {
            'Meta': {'object_name': 'Event'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end_on': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_tickets': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'promoter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.Promoter']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'start_on': ('django.db.models.fields.DateTimeField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'venue': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'zip_code': ('django.db.models.fields.IntegerField', [], {'max_length': '10'})
        },
        u'stores.billingaddress': {
            'Meta': {'object_name': 'BillingAddress'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'street_address_1': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'street_address_2': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"}),
            'zip_code': ('django.db.models.fields.IntegerField', [], {'max_length': '6'})
        },
        u'stores.cart': {
            'Meta': {'object_name': 'Cart'},
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"})
        },
        u'stores.cartitem': {
            'Meta': {'object_name': 'CartItem'},
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.Cart']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'total': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'})
        },
        u'stores.genericproduct': {
            'Meta': {'object_name': 'GenericProduct'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'depth': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'height': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'})
        },
        u'stores.genericproductvariant': {
            'Meta': {'object_name': 'GenericProductVariant'},
            'can_backorder': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'generic_product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.GenericProduct']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'stock': ('django.db.models.fields.IntegerField', [], {})
        },
        u'stores.order': {
            'Meta': {'object_name': 'Order'},
            'balance': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'total': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"})
        },
        u'stores.orderitem': {
            'Meta': {'object_name': 'OrderItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.Order']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'total': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'})
        },
        u'stores.shippingaddress': {
            'Meta': {'object_name': 'ShippingAddress'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'directions': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'street_address_1': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'street_address_2': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.BaseUser']"}),
            'zip_code': ('django.db.models.fields.IntegerField', [], {'max_length': '6'})
        },
        u'stores.ticket': {
            'Meta': {'object_name': 'Ticket'},
            'depth': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Event']"}),
            'height': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'})
        },
        u'stores.ticketvariant': {
            'Meta': {'object_name': 'TicketVariant'},
            'can_backorder': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'seating': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'stock': ('django.db.models.fields.IntegerField', [], {}),
            'ticket': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.Ticket']"})
        },
        u'stores.tshirt': {
            'Meta': {'object_name': 'Tshirt'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            'depth': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'height': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': "'1.00'", 'max_digits': '5', 'decimal_places': '2'})
        },
        u'stores.tshirtvariant': {
            'Meta': {'object_name': 'TshirtVariant'},
            'can_backorder': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'size': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'stock': ('django.db.models.fields.IntegerField', [], {}),
            'tshirt': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stores.Tshirt']"})
        }
    }

    complete_apps = ['stores']