from decimal import Decimal

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db import models

class Product(models.Model):
    """ Stuff that can go into a shopping cart """
    name = models.CharField(max_length=140)
    description = models.TextField()
    slug = models.SlugField()

    # Mass
    weight = models.DecimalField(max_digits=5, decimal_places=2, default=Decimal("1.00"))

    # Dimentions
    height = models.DecimalField(max_digits=5, decimal_places=2, default=Decimal("1.00"))
    width = models.DecimalField(max_digits=5, decimal_places=2, default=Decimal("1.00"))
    depth = models.DecimalField(max_digits=5, decimal_places=2, default=Decimal("1.00"))

    class Meta:
        abstract = True


class Variant(models.Model):
    """ Sizes and stuff """
    sku = models.CharField(max_length=255, unique=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock = models.IntegerField()
    can_backorder = models.BooleanField(default=False)

    class Meta:
        abstract = True


class Address(models.Model):
    """ Billing and shipping addresses abstract model """
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)
    street_address_1 = models.CharField(max_length=140)
    street_address_2 = models.CharField(max_length=140)
    city = models.CharField(max_length=140)
    state = models.CharField(max_length=50)
    zip_code = models.IntegerField(max_length=6)
    phone_number = models.CharField(max_length=20)

    class Meta:
        abstract = True


class Item(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    product_variant = generic.GenericForeignKey()
    quantity = models.IntegerField()
    total = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        abstract = True
