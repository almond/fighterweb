from django import forms
from .models import TicketVariant


class TicketVariantForm(forms.ModelForm):
    class Meta:
        model = TicketVariant
        exclude = ("ticket", "can_backorder", )