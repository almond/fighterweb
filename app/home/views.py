from django.views.generic.base import TemplateView
from events.models import FightType


class IndexView(TemplateView):
    template_name = "home/index.html"

    def get_context_data(self, *args, **kwargs):
        """ Need to get fight types for the dropdown """
        context = super(IndexView, self).get_context_data(*args, **kwargs)
        context["fighter_types"] = FightType.objects.all()
        return context

