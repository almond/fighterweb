from django.conf.urls import patterns, url
from .views import news_from_rss


urlpatterns = patterns('', 
    url(r'^$', news_from_rss, name='news_home'),
)
