from django.shortcuts import render
import feedparser
import datetime

def news_from_rss(request):
	# Hard Code the URL for the feed
	feed_url = "http://www.mmafighting.com/rss/current"
	feed = feedparser.parse(feed_url)
	entries = feed['entries']
	for entry in entries:		
		pub_date = entry.published_parsed
		published = datetime.date(pub_date[0], pub_date[1], pub_date[2])
		content_parsed = entry.content[0]['value']
		entry['published_datetime'] = published
		entry['content_parsed'] = content_parsed
	return render(request, 'news/default.html', {"entries": entries})
