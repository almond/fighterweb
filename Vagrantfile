# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant::Config.run do |config|
  config.vm.box = "precisex86_64"
  config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  config.vm.network :hostonly, "10.0.0.20"

  config.vm.forward_port 5432, 15432  # PostgreSQL

  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = ["chef/cookbooks", "vagrant/cookbooks"]
    chef.add_recipe "apt"
    chef.add_recipe "yum"
    chef.add_recipe "openssl"
    chef.add_recipe "build-essential"
    chef.add_recipe "postgresql"
    chef.add_recipe "main"

    chef.json = {
      :postgresql => {
        :version => '9.1',
        :pg_hba => [
          # Connections from outside the VM
          {
            :type => 'host', :db => 'all', :user => 'all', :addr => '0.0.0.0/0', :method => 'trust',
          },
          {
            :type => 'host', :db => 'all', :user => 'all', :addr => '::1/0', :method => 'trust',
          },
          # Connections from the VM itself
          {
            :type => 'local', :db => 'all', :user => 'all', :addr => nil, :method => 'trust',
          }
        ],
        :password => {
          :postgres => 'root'
        },
        :config => {
          :listen_addresses => '*',
        }
      },
      :run_list => [
        "recipe[apt]",
        "recipe[postgresql::server]",
        "recipe[postgis::default]",
        "recipe[main]",
      ]
    }
  end
end
