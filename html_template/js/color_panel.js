/*!
 * jQuery Cookie Plugin
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function($) {
    $.cookie = function(key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) { return s; } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
})(jQuery);

$(document).ready(function(){
	
	if($.cookie("cookie_color")) {
		set_color = $.cookie("cookie_color");
	}
	else {
		set_color = 'color-default';
	}
	if (set_color == 'color-default') {
		$('head').append('<link rel="stylesheet" type="text/css" href="css/theme_settings.css" id="theme_color">');
	} else {
		$('head').append('<link rel="stylesheet" type="text/css" href="css/colors/' + set_color + '.css" id="theme_color">');
	}

	//Start Control Panel Script
	$('body').append('<div class="demo_panel opacity showed"><a href="javascript:void(0)" class="panel_toggler"></a><span class="panel_title">Settings</span><div class="demo_panel_body"></div></div>');
	demo_panel_main = $('body').find('.demo_panel');
	demo_panel = $('body').find('.demo_panel_body');
	
	demo_panel.append('<div class="panel_item color_panel"><div class="panel_sub-title">Predefined Colors</div><ul class="color_list"><li class="color_item"><a class="color1" rel="color-default" href="javascript:void(0)"></a></li><li class="color_item"><a class="color2" rel="color-set2" href="javascript:void(0)"></a></li><li class="color_item"><a class="color3" rel="color-set3" href="javascript:void(0)"></a></li><li class="color_item"><a class="color4" rel="color-set4" href="javascript:void(0)"></a></li></ul><div class="clear"></div><div/>');

	demo_panel.append('<div class="panel_layouts"><div class="panel_sub-title">Layout</div><div class="panel_item layout_item"><a href="javascript:void(0)" class="layout_default">Default</a></div><div class="panel_item layout_item"><a href="javascript:void(0)" class="layout_user_bg">Boxed</a></div><div class="panel_item layout_item"><a href="javascript:void(0)" class="layout_user_image">Background Image</a></div></div>');
	
	demo_panel.append('<div class="panel_patterns"><div class="panel_sub-title">Background Color &amp; Pattern</div><ul class="pattern_list"><li class="pattern_item"><a class="pattern1" rel="bg_pattern1" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern2" rel="bg_pattern2" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern3" rel="bg_pattern3" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern4" rel="bg_pattern4" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern5" rel="bg_pattern5" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern6" rel="bg_pattern6" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern7" rel="bg_pattern7" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern8" rel="bg_pattern8" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern9" rel="bg_pattern9" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern10" rel="bg_pattern10" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern11" rel="bg_pattern11" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern12" rel="bg_pattern12" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern13" rel="bg_pattern13" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern14" rel="bg_pattern14" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern15" rel="bg_pattern15" href="javascript:void(0)"></a></li><li class="pattern_item"><a class="pattern16" rel="bg_pattern16" href="javascript:void(0)"></a></li></ul><div class="clear"></div></div>');

	demo_panel.append('<div class="panel_images"><div class="panel_sub-title">Background Image</div><ul class="bgimg_list"><li class="bgimg_item"><a class="item_img1" rel="bg_img1" href="javascript:void(0)"></a></li><li class="bgimg_item"><a class="item_img2" rel="bg_img2" href="javascript:void(0)"></a></li><li class="bgimg_item"><a class="item_img3" rel="bg_img3" href="javascript:void(0)"></a></li><li class="bgimg_item"><a class="item_img4" rel="bg_img4" href="javascript:void(0)"></a></li></ul><div class="clear"></div></div>');
		
	$('.panel_toggler').live('click', function(){
		demo_panel_main.toggleClass('showed');
	});

	$('.color_item a').live('click', function(){
		$('.color_item').find('.current').removeClass('current');
		$(this).addClass('current');
		set_color = $(this).attr('rel');

		if (set_color == 'color-default') {
			$('#theme_color').attr('href', 'css/theme_settings.css');
		} else {
			$('#theme_color').attr('href', 'css/colors/' + set_color + '.css');
		}		
		$.cookie("cookie_color", set_color);
	});

	$('.pattern_item a').live('click', function(){
		$('.pattern_item').find('.current').removeClass('current');
		$(this).addClass('current');
		if ($('html').hasClass('user_bg_layout')) {
			if ($('html').hasClass('user_pic_layout')) {
				$('html').removeClass('user_pic_layout');
				$('.custom_bg_cont').remove();
			} else {
				$('.custom_bg_cont').remove();
			}
			$('html').removeClass('user_bg_layout');
		}
		$('html').addClass('user_bg_layout');
		$('body').append('<div class="custom_bg_cont '+$(this).attr('rel')+'"/>');
	});

	$('.bgimg_item a').live('click', function(){
		$('.bgimg_item').find('.current').removeClass('current');
		$(this).addClass('current');
		$('.custom_bg_cont').remove();
		$('html').removeClass('user_bg_layout');
		$('html').removeClass('user_pic_layout');

		$('html').addClass('user_bg_layout');
		$('html').addClass('user_pic_layout');
		$('body').append('<div class="custom_bg_cont bg_pic '+$(this).attr('rel')+'"/>');
	});
	
	$('.layout_user_bg').live('click', function(){
		$('.panel_layouts').find('.current').removeClass('current');
		$('.panel_images').slideUp(400);
		$('.panel_patterns').slideDown(400);
		$(this).addClass('current');
	});
	$('.layout_user_image').live('click', function(){
		$('.panel_layouts').find('.current').removeClass('current');
		$('.panel_images').slideDown(400);
		$('.panel_patterns').slideUp(400);
		$(this).addClass('current');
	});

	$('.layout_default').live('click', function(){
		$('.panel_layouts').find('.current').removeClass('current');
		$('.panel_images').slideUp(400);
		$('.panel_patterns').slideUp(400);
		$(this).addClass('current');
		$('html').removeClass('user_bg_layout');
		$('html').removeClass('user_pic_layout');
		$('.custom_bg_cont').remove();
	});
	
});

$(window).load(function(){
	setTimeout("$('body').find('.demo_panel').removeClass('opacity')",800);
	setTimeout("$('body').find('.demo_panel').removeClass('showed')",1800);
	if ($('html').hasClass('user_bg_layout')) {
		if ($('html').hasClass('user_pic_layout')) {
			$('html').find('.layout_user_image').click();
		} else {
			$('html').find('.layout_user_bg').click();
		}
	} else {
		$('html').find('a.layout_default').addClass('current');
	}
});
