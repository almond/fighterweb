# Setup
1. Install Ruby
2. Install RVM https://rvm.io/
3. Install bundler http://bundler.io/ (ruby)
4. Go to chef folder and read README.md
5. Install Virtual Box https://www.virtualbox.org/
6. Install Vagrant http://www.vagrantup.com/
7. Go to main folder
8. $vagrant up
9. Install python
10. Install setuptools python
11. Install pip (setuptools-python)
12. Install virtualenv (python-pip)
13. Create a virtualenv venv inside root fighterweb folder
14. Activate venv $source venv/bin/activate
15. pip install -r requirements.txt
